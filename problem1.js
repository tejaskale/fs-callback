const fs = require("fs");

function problem1(){
  fs.mkdir("./jsonDirectory", { recursive: true }, (err) => {
    if (err) throw err;
    else {
      console.log("Folder is created");
      let data = [];
      for (let i = 0; i <= 10; i++) {
        data.push({
          number: i,
          square: i * i,
        });
      }
      
      
      fs.writeFile(
        "./jsonDirectory/jsonFile.json",
        JSON.stringify(data),
        (err) => {
          if (err) console.log(err);
          else {
            console.log("File written successfully");
            
  
            fs.unlink("./jsonDirectory/jsonFile.json", (err) => {
              if (err) {
                console.log(err);
              } else {
                console.log("deleted succesfully");
              }
            });
          }
        }
      );
    }
  });  
}

module.exports=problem1;