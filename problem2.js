const fs = require("fs");

function problem2() {
  fs.readFile("./lipsum.txt", "utf8", function (err, data) {
    if (err) {
      console.log(err);
    } else {
      console.log(data);

      fs.writeFile("./upperCaseFile.txt", data.toUpperCase(), (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data converted to Uppercase Succesfully");

          fs.writeFile("./filenames.txt", "upperCaseFile.txt", (err) => {
            if (err) {
              console.log(err);
            }
          });


          fs.readFile("./upperCaseFile.txt", "utf8", function (err, data) {
            if (err) {
              console.log(err);
            } else {
              console.log(data);


              fs.writeFile("./lowerCaseFile.txt", data.toLowerCase(), (err) => {
                if (err) {
                  console.log(err);
                }
                else{
                  console.log("Lower case file created successfully");
                }
              });


              fs.appendFile(
                "./filenames.txt",
                " lowerCaseFile.txt",
                "utf-8",
                (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log("Filename appended successfully");
                  }
                }
              );


              fs.readFile("./lowerCaseFile.txt", "utf-8", () => {
                if (err) {
                  console.log(err);
                } else {
                  console.log(data);
                }
              });


              let lcFile = require("./lowerCaseFile.txt");
              lcFile = lcFile.toString();

              fs.writeFile(
                "./sortFile.txt",
                lcFile.toLowerCase().split(" ").sort().join(" "),
                (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log(data);
                  }
                }
              );


              fs.appendFile(
                "./filenames.txt",
                " sortFile.txt",
                "utf-8",
                (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log("Filename appended successfully");
                  }
                }
              );
            }
          });
        }
      });
    }
  });
  

 setTimeout(()=>{
  fs.readFile("./filenames.txt", "utf-8", (err,data) => {
    if (err) {
      console.log(err);
    } else {
      const files= data.split(' ')
      files.forEach(element => {
        if(data.includes(element)){
          fs.unlink( `./${element}` , (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("deleted succesfully");
            }
          });
        }
        
      })
    }
  });
 }, 1000)

}


module.exports=problem2
